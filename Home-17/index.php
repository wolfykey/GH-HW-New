<?php
    require('connection.php');

    if ( isset($_SESSION['loggedUser']) ) {
        // array for our errors
        $errorsArr = array();

        $loggedUser = $_SESSION['loggedUser'];
//        var_dump($loggedUser);

        $username = $loggedUser['username'];
        $firstName = $loggedUser['firstName'];
        $lastName = $loggedUser['lastName'];

        //make post
        $data = $_POST;
        if ( isset($data['makePost']) ) {

            //check postTitle
            if ( trim($data['postTitle']) == '' ) {
                $errorsArr[] = 'Title can\'t be empty!';
            }

            //check postDescription
            if ( trim($data['postDescription']) == '' ) {
                $errorsArr[] = 'Description can\'t be empty!';
            }

            $id = $loggedUser['id'];
            $title = $data['postTitle'];
            $description = $data['postDescription'];

            // insert new post to table
            $sql = "INSERT INTO posts (id, title, text, dateAdd) VALUES ('$id', '$title', '$description', NOW())";
            $result = $connection->query($sql);

            if ($result) {
                $sucMess = 'Post added successfully!';
            } else {
                $errorsArr[] = 'Post added false!';
            }
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Title Page-->
    <title>
        Vovk Network
    </title>

    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,700"
          rel="stylesheet">

    <!-- Main CSS-->
    <link rel="stylesheet" href="assets/css/main.css">
</head>
<body>
<div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
    <div class="wrapper wrapper--w680">
        <div class="card card-4  m-b-40">
            <div class="card-body">
                <?php
                if ( isset($_SESSION['loggedUser']) ) {
                ?>
                    <h2 class="title">Hello, <?php echo($firstName . ' ' . $lastName ); ?></h2>
                    <h3 class="title m-b-40"><?php echo('@' . $username); ?></h3>
                    <div class="p-t-15">
                        <a class="btn btn--secondary btn--radius-2 w-100" href="logout.php">
                            Logout
                        </a>
                    </div>
                    <h2 class="title p-t-130">Want add new post?</h2>
                    <form method="POST" action="">
                        <?php if ( isset($sucMess) ) { ?>
                            <div class="alert alert-success">
                                <?php
                                echo $sucMess;
                                ?>
                            </div>
                        <?php } ?>
                        <?php if ( isset($errorMess) ) { ?>
                            <div class="alert alert-danger">
                                <?php
                                echo $errorMess;
                                ?>
                            </div>
                        <?php } ?>
                        <div class="row row-space">
                            <div class="col-12">
                                <div class="input-group">
                                    <label class="label">Title</label>
                                    <input class="input--style-4" type="text"
                                           name="postTitle"
                                           placeholder="e.g. $ = 8 UAH">
                                </div>
                            </div>
                        </div>
                        <div class="row row-space">
                            <div class="col-12">
                                <div class="input-group">
                                    <label class="label">Description</label>
                                    <textarea class="input--style-4 w-100"
                                              type="text"
                                              name="postDescription"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="p-t-15">
                            <button class="btn btn--radius-2 btn--green w-100"
                                    name="makePost" type="submit">
                                Publish
                            </button>
                        </div>
                    </form>
                <?php } else { ?>
                    <h2 class="title">Hello, what do you want?</h2>
                    <div class="row row-space">
                        <div class="col-6">
                            <a href="login.php" class="btn btn--radius-2 btn--blue w-100">
                                Login
                            </a>
                        </div>
                        <div class="col-6">
                            <a href="signup.php" class="btn btn--radius-2 btn--green w-100">
                                Registration
                            </a>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <?php
        if ( isset($_SESSION['loggedUser']) ) {

            // get data
            $id = $loggedUser['id'];
            $sql = "SELECT id, title, text, dateAdd FROM posts WHERE id = '$id'";
            $result = $connection->query($sql);

            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    ?>
                    <div class="card card-4 m-b-40">
                        <div class="card-body">
                            <h2 class="title"><?php echo($row["title"]); ?></h2>
                            <div class="p-t-15">
                                <p><?php echo($row["text"]); ?></p>
                            </div>
                            <h3 class="title-s"><?php echo($row["dateAdd"]); ?></h3>
                        </div>
                    </div>
                    <?php
                }
            } else {
                ?>
                <div class="card card-4 m-b-40">
                    <div class="card-body">
                        <h2 class="title">You don't have any post!</h2>
                    </div>
                </div>
                <?php
            }
        } ?>
    </div>
</div>

<script src="assets/js/libs.js"></script>
<script src="assets/js/main.js"></script>
</body>
</html>
