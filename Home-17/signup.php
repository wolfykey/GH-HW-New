<?php
    require('connection.php');

    //sign up
    $data = $_POST;
    if ( isset($data['make_signup']) ) {
        // array for our errors
        $errorsArr = array();

        //check username
        if ( trim($data['username']) == '' ) {
            $errorsArr[] = 'Username can\'t be empty!';
        }

        //check exists user or no
        $undefUser = trim($data['username']);
        $sql = "SELECT * FROM users WHERE username = '$undefUser'";

        if ( mysqli_num_rows($connection->query($sql)) > 0 ) {
            $errorsArr[] = 'Login already exists. Select another login!';
        }

        //check email
        if ( trim($data['email']) == '' ) {
            $errorsArr[] = 'Email can\'t be empty!';
        }

        //check exists email or no
        $undefEmail = trim($data['email']);

        $sql = "SELECT * FROM users WHERE email = '$undefEmail'";

        if (mysqli_num_rows($connection->query($sql)) > 0) {
            $errorsArr[] = 'Email already exists. Select another email!';
        }

        //check userPassword
        if ( $data['userPassword'] == '' ) {
            $errorsArr[] = 'Password can\'t be empty!';
        }

        //check userPasswordRepeat
        if ( $data['userPasswordRepeat'] != $data['userPassword'] ) {
            $errorsArr[] = 'Password repeat not equal first password!';
        }

        //check firstName
        if ( trim($data['firstName']) == '' ) {
            $errorsArr[] = 'First name can\'t be empty!';
        }

        //check lastName
        if ( trim($data['lastName']) == '' ) {
            $errorsArr[] = 'Last name can\'t be empty!';
        }

        //check userAge
        if ( trim($data['userAge']) == '' ) {
            $errorsArr[] = 'Age can\'t be empty!';
        }

        //check array for errors
        if ( empty($errorsArr) ) {
            //all is fine, can sign up this user

            $username = $data['username'];
            $email = $data['email'];
            $userPassword = password_hash($data['userPassword'], PASSWORD_DEFAULT);
            $firstName = $data['firstName'];
            $lastName = $data['lastName'];
            $userAge = $data['userAge'];
            $gender = $data['gender'];

            // insert new user to table
            $query = "INSERT INTO users (username, email, password, firstName, lastName, age, gender) 
                      VALUES('$username', '$email', '$userPassword', '$firstName', '$lastName', '$userAge', '$gender')";

            $result = mysqli_query($connection, $query);

            if ($result) {
                $sucMess = 'Registration successfully!';
            } else {
                $errorsArr[] = 'Registration false!';
            }

        } else {
            //echo errors
            $errorMess = array_shift($errorsArr);
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Title Page-->
    <title>
        Vovk Network - Sign Up
    </title>

    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,700"
          rel="stylesheet">

    <!-- Main CSS-->
    <link rel="stylesheet" href="assets/css/main.css">
</head>
<body>
<div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
    <div class="wrapper wrapper--w680">
        <div class="card card-4">
            <div class="card-body">
                <h2 class="title">Registration Form</h2>
                <form method="POST" action="signup.php">
                    <?php if ( isset($sucMess) ) { ?>
                        <div class="alert alert-success">
                            <?php
                            echo $sucMess;
                            ?>
                        </div>
                    <?php } ?>
                    <?php if ( isset($errorMess) ) { ?>
                        <div class="alert alert-danger">
                            <?php
                                echo $errorMess;
                            ?>
                        </div>
                    <?php } ?>
                    <div class="row row-space">
                        <div class="col-6">
                            <div class="input-group">
                                <label class="label">User name</label>
                                <input class="input--style-4" type="text"
                                       name="username" placeholder="e.g. wolfykey"
                                       value="<?php echo @$data['username'] ?>">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group">
                                <label class="label">Email</label>
                                <input class="input--style-4" type="email"
                                       name="email" placeholder="e.g. vladyslav@gmail.com"
                                       value="<?php echo @$data['email'] ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row row-space">
                        <div class="col-6">
                            <div class="input-group">
                                <label class="label">Password</label>
                                <input class="input--style-4" type="password"
                                       name="userPassword">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group">
                                <label class="label">Repeat password</label>
                                <input class="input--style-4" type="password"
                                       name="userPasswordRepeat">
                            </div>
                        </div>
                    </div>
                    <div class="row row-space">
                        <div class="col-6">
                            <div class="input-group">
                                <label class="label">first name</label>
                                <input class="input--style-4" type="text"
                                       name="firstName" placeholder="e.g. Vladyslav"
                                       value="<?php echo @$data['firstName'] ?>">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group">
                                <label class="label">last name</label>
                                <input class="input--style-4" type="text"
                                       name="lastName" placeholder="e.g. Vovk"
                                       value="<?php echo @$data['lastName'] ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row row-space">
                        <div class="col-6">
                            <div class="input-group">
                                <label class="label">Age</label>
                                <input class="input--style-4" type="text"
                                       name="userAge" placeholder="e.g. 23"
                                       value="<?php echo @$data['userAge'] ?>">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group">
                                <label class="label">Gender</label>
                                <div class="p-t-10">
                                    <label class="radio-container m-r-45">
                                        Male
                                        <input type="radio" checked="checked"
                                               name="gender" value="male">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="radio-container">
                                        Female
                                        <input type="radio" name="gender"
                                               value="female">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="p-t-15">
                        <button class="btn btn--radius-2 btn--green w-100"
                                name="make_signup" type="submit">
                            Registration
                        </button>
                    </div>
                    <div class="p-t-15">
                        <a class="btn btn--secondary btn--radius-2 w-100" href="/">
                            Go to home
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="assets/js/libs.js"></script>
<script src="assets/js/main.js"></script>
</body>
</html>
