<?php
    require('connection.php');

    //login
    $data = $_POST;
    if ( isset($data['makeLogin']) ) {
        // array for our errors
        $errorsArr = array();

        $undefUser = trim($data['username']);
        $password = trim($data['userPassword']);

        // select user from DB
        $user = "SELECT * FROM users WHERE username = '$undefUser'";

        //check exists user or no
        if ( mysqli_num_rows($connection->query($user)) > 0 ) {
            // check password
            $result = $connection->query($user);
            $row = $result->fetch_assoc();

            if ( password_verify($password, $row['password']) ) {
                // remember our user in session
                $_SESSION['loggedUser'] = $row;
                $sucMess = 'You are successfully login!';
                header('Location: /');
            } else {
                $errorsArr[] = 'Password not valid!';
            }
        } else {
            $errorsArr[] = 'This user not found!';
        }

        //check array for errors
        if ( !empty($errorsArr) ) {
            //echo errors
            $errorMess = array_shift($errorsArr);
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Title Page-->
    <title>
        Vovk Network - Login
    </title>

    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,700"
          rel="stylesheet">

    <!-- Main CSS-->
    <link rel="stylesheet" href="assets/css/main.css">
</head>
<body>
<div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
    <div class="wrapper wrapper--w680">
        <div class="card card-4">
            <div class="card-body">
                <h2 class="title">Login</h2>
                <form method="POST" action="login.php">
                    <?php if ( isset($sucMess) ) { ?>
                        <div class="alert alert-success">
                            <?php
                            echo $sucMess;
                            ?>
                        </div>
                    <?php } ?>
                    <?php if ( isset($errorMess) ) { ?>
                        <div class="alert alert-danger">
                            <?php
                            echo $errorMess;
                            ?>
                        </div>
                    <?php } ?>
                    <div class="row row-space">
                        <div class="col-6">
                            <div class="input-group">
                                <label class="label">User name</label>
                                <input class="input--style-4" type="text"
                                       name="username" placeholder="e.g. wolfykey"
                                       value="<?php echo @$data['username'] ?>">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group">
                                <label class="label">Password</label>
                                <input class="input--style-4" type="password"
                                       name="userPassword">
                            </div>
                        </div>
                    </div>
                    <div class="p-t-15">
                        <button class="btn btn--radius-2 btn--green w-100"
                                name="makeLogin" type="submit">
                            Login
                        </button>
                    </div>
                    <div class="p-t-15">
                        <a class="btn btn--secondary btn--radius-2 w-100" href="/">
                            Go to home
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="assets/js/libs.js"></script>
<script src="assets/js/main.js"></script>
</body>
</html>
