<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Title Page-->
    <title>DATABASE - in our hearts</title>

    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,700"
          rel="stylesheet">

    <!-- Main CSS-->
    <link rel="stylesheet" href="assets/css/main.css">
</head>
<body>
<?php
    require('connection.php');

    if (isset($_POST)) {
        $username = $_POST['username'];
        $email = $_POST['email'];
        if ($_POST['userPassword'] === $_POST['userPasswordRepeat']) {
            $userPassword = md5($_POST['userPassword']);
        }
        $firstName = $_POST['firstName'];
        $lastName = $_POST['lastName'];
        $userAge = $_POST['userAge'];
        $gender = $_POST['gender'];

        $query = "INSERT INTO users (username, email, password, firstName, lastName, age, gender) 
                  VALUES('$username', '$email', '$userPassword', '$firstName', '$lastName', 
                  '$userAge', '$gender')";

        $result = mysqli_query($connection, $query);

        if ($result) {
            $sucmsg = "Registration successfully!";
        } else {
            $falsemsg = "Registration false!";
        }
    }
?>

<div class="page-wrapper bg-gra-02 p-t-130 p-b-100 font-poppins">
    <div class="wrapper wrapper--w680">
        <div class="card card-4">
            <div class="card-body">
                <h2 class="title">Registration Form</h2>
                <form method="POST" action="/">
                    <?php if(isset($sucmsg)) { ?>
                        <div class="alert alert-success">
                            <?php
                                echo $sucmsg;
                            ?>
                        </div>
                    <?php } ?>
                    <?php if(isset($falsemsg)) { ?>
                        <div class="alert alert-danger">
                            <?php
                            echo $falsemsg;
                            ?>
                        </div>
                    <?php } ?>
                    <div class="row row-space">
                        <div class="col-6">
                            <div class="input-group">
                                <label class="label">User name</label>
                                <input class="input--style-4" type="text" name="username" placeholder="e.g. wolfykey"
                                       required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group">
                                <label class="label">Email</label>
                                <input class="input--style-4" type="email" name="email" placeholder="e.g. vladyslav@gmail.com" required>
                            </div>
                        </div>
                    </div>
                    <div class="row row-space">
                        <div class="col-6">
                            <div class="input-group">
                                <label class="label">Password</label>
                                <input class="input--style-4" type="password" name="userPassword" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group">
                                <label class="label">Repeat password</label>
                                <input class="input--style-4" type="password" name="userPasswordRepeat" required>
                            </div>
                        </div>
                    </div>
                    <div class="row row-space">
                        <div class="col-6">
                            <div class="input-group">
                                <label class="label">first name</label>
                                <input class="input--style-4" type="text" name="firstName" placeholder="e.g. Vladyslav" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group">
                                <label class="label">last name</label>
                                <input class="input--style-4" type="text" name="lastName" placeholder="e.g. Vovk" required>
                            </div>
                        </div>
                    </div>
                    <div class="row row-space">
                        <div class="col-6">
                            <div class="input-group">
                                <label class="label">Age</label>
                                <input class="input--style-4" type="text" name="userAge" placeholder="e.g. 23" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group">
                                <label class="label">Gender</label>
                                <div class="p-t-10">
                                    <label class="radio-container m-r-45">
                                        Male
                                        <input type="radio" checked="checked" name="gender" value="male">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="radio-container">
                                        Female
                                        <input type="radio" name="gender" value="female">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="p-t-15">
                        <button class="btn btn--radius-2 btn--blue w-100" type="submit">
                            Registration
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="assets/js/libs.js"></script>
<script src="assets/js/main.js"></script>
</body>
</html>
