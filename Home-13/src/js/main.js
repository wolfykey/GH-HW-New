$(function () {
  let tasksList = $('.tasksList')
  let taskInput = $('.taskInput')
  let notification = $('.notification')

  function displayNotification () {
    if (!tasksList.children().length) {
      notification.fadeIn(400)
      notification.removeClass('d-none')
    } else {
      notification.addClass('d-none')
    }
  }

  $('.taskAdd').on('click', function () {
    if (!taskInput.val()) {
      return false
    }

    tasksList.append('<li>' + '<button class="btn edit"><i class="fas fa-pencil-alt"></i></button>' + '<p class="taskText">' + taskInput.val() + '</p>' + '<button class="delete"><i class="fas fa-times"></i></button></li>')
    taskInput.val('')
    displayNotification()
    removeTask()
    editTask()
  })

  function removeTask () {
    $('.delete').on('click', function () {
      let parent = $(this).parent()

      parent.css('animation', 'fadeOut .3s linear')

      setTimeout(function () {
        parent.remove()
        displayNotification()
      }, 300)
    })
  }

  function editTask () {
    $('.edit').on('click', function () {
      $(this).children().remove()
      $(this).addClass('save')
      $(this).removeClass('edit')
      $(this).append('<i class="fas fa-save"></i>')

      let currentTaskText = $(this).siblings('p').text()

      $('p.taskText').replaceWith( function () {
        return '<input class="taskText" value="' + currentTaskText + '">'
      })

      //нижче баг
      //при збереженні перетераються всі інші такси
      //пофіксити не вдалось
      $('.save').on('click', function () {
        $('.taskText').replaceWith( function () {
          return '<p class="taskText">' + $('.save').parent().find('input').val() + '</p>'
        })
      })
    })
  }
})



