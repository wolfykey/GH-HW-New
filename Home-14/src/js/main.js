$(window).on('scroll', function() {
  if (window.innerWidth >= 768) {
    let scroll = $(window).scrollTop()
    if (scroll >= 2) {
      $("nav").addClass(" bg-light")
    } else {
      $("nav").removeClass(" bg-light")
    }
  }
});
