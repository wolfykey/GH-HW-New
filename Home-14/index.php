<?php
    $data = require('data.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>InteractiveAgency</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,400,700" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/main.css">
</head>
<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="/">
                <img class="main-logo" src="<?php echo ($data['siteLogo']) ?>" alt="logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <?php
                        foreach($data['mainMenu'] as $menu) {
                            ?>
                            <li class="nav-item"><a class="nav-link" href='<?php echo $menu["url"]?>'><?php echo $menu["title"] ?></a></li>
                        <?php
                        }
                    ?>
                </ul>
            </div>
        </nav>
    </header>
    <section class="hero">
        <div class="container text-center">
            <?php
            foreach($data['hero'] as $menu) {
                ?>
                <h1 class="title">
                    <?php echo $menu['title']; ?>
                </h1>
                <p class="sub-title">
                    <?php echo $menu['content']; ?>
                </p>
                <a href="<?= $data['buttonAsk']['url']; ?>" class="btn-primary">
                    <?php echo $data['buttonAsk']['title']; ?>
                </a>
                <?php
            }
            ?>
        </div>
        <img src="assets/img/scrool.png" alt="scrool icon" class="scrool">
    </section>
    <div class="best">
        <div class="row">
            <section class="col-12 col-md-6 best__section">
                <div class="container d-flex flex-column justify-content-center align-items-center align-items-md-start no-m-right">
                    <?php
                    foreach ($data['best'] as $design) {
                        ?>
                        <h2 class="title text-center text-md-left">
                            <?php echo $design['title-1']; ?>
                        </h2>
                        <p class="sub-title text-center text-md-left">
                            <?php echo $design['content-1']; ?>
                        </p>
                        <a href="<?= $data['buttonAsk']['url']; ?>" class="btn-primary">
                            <?php echo $data['buttonAsk']['title']; ?>
                        </a>
                        <?php
                    }
                    ?>
                </div>
            </section>
            <section class="col-12 col-md-6 best__section">
                <div class="container d-flex flex-column justify-content-center align-items-center align-items-md-start no-m-left">
                    <?php
                    foreach ($data['best'] as $design) {
                        ?>
                        <h2 class="title text-center text-md-left">
                            <?php echo $design['title-2']; ?>
                        </h2>
                        <p class="sub-title text-center text-md-left">
                            <?php echo $design['content-2']; ?>
                        </p>
                        <a href="<?= $data['buttonAsk']['url']; ?>" class="btn-primary">
                            <?php echo $data['buttonAsk']['title']; ?>
                        </a>
                        <?php
                    }
                    ?>
                </div>
            </section>
        </div>
    </div>
    <section class="we-do">
        <div class="container">
            <h2 class="text-center section-title">
                <?php echo $data['weDo']['title']; ?>
            </h2>
            <div class="row justify-content-between">
                <?php
                foreach ($data['weDo']['items'] as $workDirection) {
                    ?>
                    <div class="col-12 col-md-6 we-do__card d-flex flex-column flex-lg-row justify-content-center align-items-center align-items-lg-start">
                        <div class="we-do__card__icon">
                            <img src="<?php echo $workDirection['img'] ?>" alt="desktop applications">
                        </div>
                        <div class="we-do__card__content">
                            <h3 class="title text-center text-lg-left">
                                <?php echo $workDirection['title'] ?>
                            </h3>
                            <p class="description text-center text-lg-left">
                                <?php echo $workDirection['content'] ?>
                            </p>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <a href="<?php echo $data['buttonAsk']['url']; ?>" class="btn-primary m-auto">
                    <?php echo $data['buttonAsk']['title']; ?>
                </a>
            </div>
        </div>
    </section>
    <div class="our-work w-100">
        <div class="slides">
            <?php
            foreach ($data['ourWork'] as $workCase) {
                ?>
                <div class="slides__item">
                    <img class="slides__item__img d-block w-100" src="<?php echo $workCase['img'] ?>" alt="First slide">
                    <div class="slides__item__view">
                        <h2 class="title text-center">
                            <?php echo $workCase['title'] ?>
                        </h2>
                        <p class="description text-center">
                            <?php echo $workCase['content'] ?>
                        </p>
                        <a href="<?php echo $workCase['btnWatch']['url']; ?>" class="btn-primary m-auto">
                            <?php echo $workCase['btnWatch']['title']; ?>
                        </a>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
    <section class="about-us">
        <div class="container">
            <div class="heading text-center">
                <h2 class="title">
                    <?php echo $data['aboutUs']['title']; ?>
                </h2>
                <p class="description">
                    <?php echo $data['aboutUs']['content']; ?>
                </p>
            </div>
            <div class="row user-items">
                <?php
                foreach ($data['aboutUs']['users'] as $person) {
                    ?>
                    <div class="col-md-6 col-lg-3">
                        <div class="user-card text-center">
                            <img src="<?php echo $person['photo'] ?>" alt="John Doe">
                            <h3 class="user-card__title">
                                <?php echo $person['name'] ?>
                            </h3>
                            <p class="user-card__description">
                                <?php echo $person['description'] ?>
                            </p>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <a href="<?= $data['aboutUs']['btnWatch']['url']; ?>" class="btn-primary m-auto">
                    <?= $data['aboutUs']['btnWatch']['title']; ?>
                </a>
            </div>
        </div>
    </section>
    <div class="location-map">
        <iframe src="<?php echo $data['map'] ?>" allowfullscreen></iframe>
    </div>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p>
                        <?php echo $data['copy']['topDescription']; ?>
                    </p>
                    <p>
                        <?php echo $data['copy']['bottomDescription']; ?>
                    </p>
                </div>
            </div>
        </div>
    </footer>

    <script src="assets/js/libs.js"></script>
    <script src="assets/js/main.js"></script>
</body>
</html>
