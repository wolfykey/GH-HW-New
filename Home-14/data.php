<?php

return [
    'siteLogo' => '/assets/img/Logo.png',
    'mainMenu' => [
        [
            'title' => 'Home',
            'url' => '#',
        ],
        [
            'title' => 'About us',
            'url' => '#',
        ],
        [
            'title' => 'Offer',
            'url' => '#',
        ],
        [
            'title' => 'Portfolio',
            'url' => '#',
        ],
        [
            'title' => 'Contact',
            'url' => '#',
        ],
    ],
    'hero' => [
        [
            'title' => 'Lorem ipsum dolor sit amet',
            'content' => 'Nunc vel nibh tempor, pharetra lectus congue, luctus orci.',
        ],
    ],
    'buttonAsk' => [
        'title' => 'Ask for price',
        'url' => '#',
    ],
    'best' => [
        [
            'title-1' => 'Best<span class="bold">Design</span>',
            'title-2' => 'Best<span class="bold">Code</span>',
            'content-1' => 'Aliquam sagittis neque in lectus semper, nec elementum
                        arcu scelerisque. Curabitur ullamcorper auctor mauris,
                        placerat fermentum lectus vulputate sed. Phasellus
                        vestibulum sit amet dolor eget eleifend.',
            'content-2' => 'Aliquam sagittis neque in lectus semper, nec elementum
                        arcu scelerisque. Curabitur ullamcorper auctor mauris,
                        placerat fermentum lectus vulputate sed. Phasellus
                        vestibulum sit amet dolor eget eleifend.',
        ],
    ],
    'weDo' => [
        'title' => 'What we do?',
        'items' => [
            [
                'img' => 'assets/img/we-1.png',
                'title' => 'Web design',
                'content' => 'Aliquam sagittis neque in lectus semper, nec elementum arcu scelerisque.
                       Curabitur ullamcorper auctor mauris, placerat fermentum.',
            ],
            [
                'img' => 'assets/img/we-2.png',
                'title' => 'Web Applications',
                'content' => 'Aliquam sagittis neque in lectus semper, nec elementum arcu scelerisque.
                       Curabitur ullamcorper auctor mauris, placerat fermentum.',
            ],
            [
                'img' => 'assets/img/we-3.png',
                'title' => 'Digital paiting',
                'content' => 'Aliquam sagittis neque in lectus semper, nec elementum arcu scelerisque.
                       Curabitur ullamcorper auctor mauris, placerat fermentum.',
            ],
            [
                'img' => 'assets/img/we-4.png',
                'title' => 'Desktop Applications',
                'content' => 'Aliquam sagittis neque in lectus semper, nec elementum arcu scelerisque.
                       Curabitur ullamcorper auctor mauris, placerat fermentum.',
            ],
        ],
    ],
    'ourWork' => [
        [
            'img' => 'assets/img/slide-1.png',
            'title' => 'Neptune template',
            'content' => 'Aliquam sagittis neque in lectus semper, nec elementum
                        arcu scelerisque. Curabitur ullamcorper auctor mauris,
                        placerat fermentum.',
            'btnWatch' => [
                'title' => 'Watch more',
                'url' => '#',
            ],
        ],
        [
            'img' => 'assets/img/slide-2.png',
            'title' => 'Neptune template',
            'content' => 'Aliquam sagittis neque in lectus semper, nec elementum
                        arcu scelerisque. Curabitur ullamcorper auctor mauris,
                        placerat fermentum.',
            'btnWatch' => [
                'title' => 'Watch more',
                'url' => '#',
            ],
        ],
        [
            'img' => 'assets/img/slide-3.png',
            'title' => 'Neptune template',
            'content' => 'Aliquam sagittis neque in lectus semper, nec elementum
                        arcu scelerisque. Curabitur ullamcorper auctor mauris,
                        placerat fermentum.',
            'btnWatch' => [
                'title' => 'Watch more',
                'url' => '#',
            ],
        ],
        [
            'img' => 'assets/img/slide-1.png',
            'title' => 'Neptune template',
            'content' => 'Aliquam sagittis neque in lectus semper, nec elementum
                        arcu scelerisque. Curabitur ullamcorper auctor mauris,
                        placerat fermentum.',
            'btnWatch' => [
                'title' => 'Watch more',
                'url' => '#',
            ],
        ],

    ],
    'aboutUs' => [
        'title' => 'About us!',
        'content' => 'Aliquam sagittis neque in lectus semper, nec elementum arcu scelerisque.
                       Curabitur ullamcorper auctor mauris, placerat fermentum.',
        'users' => [
            [
                'photo' => 'assets/img/user-1.png',
                'name' => 'John Doe',
                'description' => 'Curabitur ullamcorper auctor mauris, placerat fermentum.',
            ],
            [
                'photo' => 'assets/img/user-2.png',
                'name' => 'John Doe',
                'description' => 'Curabitur ullamcorper auctor mauris, placerat fermentum.',
            ],
            [
                'photo' => 'assets/img/user-3.png',
                'name' => 'John Doe',
                'description' => 'Curabitur ullamcorper auctor mauris, placerat fermentum.',
            ],
            [
                'photo' => 'assets/img/user-4.png',
                'name' => 'John Doe',
                'description' => 'Curabitur ullamcorper auctor mauris, placerat fermentum.',
            ],
        ],
        'btnWatch' => [
            'title' => 'Watch more',
            'url' => '#',
        ],
    ],
    'map' => 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d387193.3059445135!2d-74.25986613799748!3d40.69714941774136!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2z0J3RjNGOLdCZ0L7RgNC6LCDQodCo0JA!5e0!3m2!1sru!2sua!4v1545407628631',
    'copy' => [
        'topDescription' => 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
        'bottomDescription' => 'Try awsome tool for desgners <a href="https://symu.co/">symu.co</a>',
    ],
];
