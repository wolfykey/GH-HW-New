<?php
    if (isset($_POST['numberStr'])) {
        $unsortedArray = explode(" ", $_POST['numberStr']);

//        це точно працює, але ж потрібно було своє написати)
//        $sortedArray = asort($unsortedArray);

        function insertionSort($unsortedArray) {
            for ($i = 1; $i < count($unsortedArray); $i++) {
                $rightValue = $unsortedArray[$i];
                $leftValue = $i - 1;
                while ($leftValue >= 0 && $unsortedArray[$leftValue] > $rightValue) {
                    $unsortedArray[$leftValue+1] = $unsortedArray[$leftValue];
                    $leftValue--;
                }
                $unsortedArray[++$leftValue] = $rightValue;
            }
            return $unsortedArray;
        }

        $sortedArray = insertionSort($unsortedArray);
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Insertion Sort</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/main.css">
</head>
<body>
    <section class="container">
        <div class="row">
            <h1>
                Сортування елементів масиву!
            </h1>
            <form action="/" method="POST" class="d-flex flex-column mt-3 mb-3">
                <h2>
                    Зверни увагу! 
                </h2>
                <p>
                    Щоб сортування працювало коректно, розділяй дані
                    <strong>одним</strong>
                    пробілом, бо не можу адекватно розібратись з trim()
                </p>
                <label for="numberStr">
                    Введіть цифри для сортування:
                </label>
                <input id="numberStr" class="input-array" type="text" name="numberStr" placeholder="input your array">
                <input type="submit" class="btn-primary">
            </form>
        </div>
        <?php if (isset($sortedArray)): ?>
            <div class="row d-flex flex-column">
                <h2>Дані, які були введені користувачем:</h2>
                <p>Від меншого до більшого</p>
                <ul class="inputData">
                    <?php
                    foreach ($sortedArray as $number) {
                        ?>
                        <li><?php echo $number ?></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        <?php endif; ?>
    </section>

    <script src="assets/js/libs.js"></script>
    <script src="assets/js/main.js"></script>
</body>
</html>
