<?php
    include("connection.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Title Page-->
    <title>DATABASE - in our hearts - 2</title>

    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,700"
          rel="stylesheet">

    <!-- Main CSS-->
    <link rel="stylesheet" href="assets/css/main.css">
</head>
<body>
    <div class="container">
        <?php
        //first task
        $sql = "SELECT * FROM block WHERE theme = 'bartik' AND module = 'system' ";
        $result = $connection->query($sql);

        if ($result->num_rows > 0) {
            ?>
            <p>
                1. Get all blocks from block table where theme is bartik and module is system:
            </p>
            <ul>
                <?php
                while($row = $result->fetch_assoc()) {
                    echo "<li>" .
                        "<i>bid:</i> " . $row["bid"] . "<br>" .
                        "<i>module:</i> " . $row["module"] . "<br>" .
                        "<i>delta:</i> " . $row["delta"] . "<br>" .
                        "<i>theme:</i> " . $row["theme"] . "<br>" .
                        "<i>status:</i> " . $row["status"] . "<br>" .
                        "<i>weight:</i> " . $row["weight"] . "<br>" .
                        "<i>region:</i> " . $row["region"] . "<br>" .
                        "<i>custom:</i> " . $row["custom"] . "<br>" .
                        "<i>visibility:</i> " . $row["visibility"] . "<br>" .
                        "<i>title:</i> " . $row["title"] . "<br>" .
                        "<i>cache:</i> " . $row["cache"] . "<br>" .
                        "</li><br>";
                }
                ?>
            </ul>
            <?php
        } else {
            echo "0 results";
        }

        //second task
        $sql = "SELECT * FROM node WHERE type = 'delivery' AND title LIKE '8046%' AND DATE_FORMAT(FROM_UNIXTIME(created), '%M') = 'October'";
        $result = $connection->query($sql);

        if ($result->num_rows > 0) {
            ?>
            <p>
                2. Get nodes where type is delivery and all that made in october and title begins with 8046:
            </p>
            <ul>
                <?php
                while($row = $result->fetch_assoc()) {
                    echo "<li>" .
                        "<i>nid:</i> " . $row["nid"] . "<br>" .
                        "<i>vid:</i> " . $row["vid"] . "<br>" .
                        "<i>type:</i> " . $row["type"] . "<br>" .
                        "<i>language:</i> " . $row["language"] . "<br>" .
                        "<i>title:</i> " . $row["title"] . "<br>" .
                        "<i>uid:</i> " . $row["uid"] . "<br>" .
                        "<i>status:</i> " . $row["status"] . "<br>" .
                        "<i>created:</i> " . $row["created"] . "<br>" .
                        "<i>changed:</i> " . $row["changed"] . "<br>" .
                        "<i>comment:</i> " . $row["comment"] . "<br>" .
                        "<i>promote:</i> " . $row["promote"] . "<br>" .
                        "<i>sticky:</i> " . $row["sticky"] . "<br>" .
                        "<i>tnid:</i> " . $row["tnid"] . "<br>" .
                        "<i>translate:</i> " . $row["translate"] . "<br>" .
                        "</li><br>";
                }
                ?>
            </ul>
            <?php
        } else {
            echo "0 results";
        }

        //third task
        $sql = "SELECT node.nid, node.title, users.name, users.mail FROM node LEFT JOIN users ON node.uid = users.uid WHERE users.name = 'serhiy' ORDER BY node.created DESC LIMIT 20";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            ?>
            <p>
                3. Get user name and nodes that where published by user 'serhiy'(output username and email with each node). get last 20 nodes:
            </p>
            <ul>
                <?php
                while($row = $result->fetch_assoc()) {
                    echo "<li>" .
                        "<i>nid:</i> " . $row["nid"] . "<br>" .
                        "<i>title:</i> " . $row["title"] . "<br>" .
                        "<i>name:</i> " . $row["name"] . "<br>" .
                        "<i>mail:</i> " . $row["mail"] . "<br>" .
                        "</li><br>";
                }
                ?>
            </ul>
            <?php
        } else {
            echo "0 results";
        }

        //fourth task
        $sql = "SELECT * FROM variable WHERE name LIKE '%cache%' AND name != 'cache' ";
        $result = $connection->query($sql);

        if ($result->num_rows > 0) {
            ?>
            <p>
                4. Get all variable name that has cache word(cache_akjsgdkjag) but not (cache)(see variable table):
            </p>
            <ul>
                <?php
                while($row = $result->fetch_assoc()) {
                    echo "<li>" .
                        "<i>name:</i> " . $row["name"] . "<br>" .
                        "<i>value:</i> " . $row["value"] . "<br>" .
                        "</li><br>";
                }
                ?>
            </ul>
            <?php
        } else {
            echo "0 results";
        }

        ?>
    </div>

<script src="assets/js/libs.js"></script>
<script src="assets/js/main.js"></script>
</body>
</html>
